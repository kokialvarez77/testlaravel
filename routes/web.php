<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/home');
});

Route::get('/', 'HomeController@index');


// Route::get('articles/{nombre?}', function($nombre = "kkk") {
// 	echo('El nombre que has colocado es: '.$nombre);
// });

// Route::group(['prefix' => 'articles'], function(){
// 	Route::get('view/{id}', [
// 		'uses' => 'TestController@view',
// 		'as' => 'articlesView'
// 		]);
// });

Route::group(['prefix' => 'admin'], function(){

	Route::resource('users','UsersController');
	Route::get('users/{id}/destroy', [
		'uses' => 'UsersController@destroy',
		'as'   => 'admin.users.destroy'
		]);
	Route::get('users/{id}/edit', [
		'uses' => 'UsersController@edit',
		'as'   => 'admin.users.edit'
		]);

	Route::resource('categories', 'CategoriesController');
	Route::get('categories/{id}/destroy', [
		'uses'	=> 'CategoriesController@destroy',
		'as'	=> 'admin.categories.destroy'
		]);
	Route::get('categories/{id}/edit', [
		'uses' 	=> 'CategoriesController@edit',
		'as'	=> 'admin.categories.edit'
		]);

});
Auth::routes();

//Route::get('/home', 'HomeController@index');
