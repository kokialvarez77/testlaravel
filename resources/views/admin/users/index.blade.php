@extends('admin.template.main')

@section('title', 'Lista de usuarios')

@section('contenido')
	<a href="{{ route('users.create') }}" class="btn btn-info"> Nuevo Usuario </a> <br>
	<div class="table-responsive">
	  <table class="table table-striped">
	  	<thead>
		    <th> ID </th>
		    <th> Nombre </th>
		    <th> Tipo </th>
		    <th> Correo </th>
		    <th> ACCION </th>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->type }}</td>
					<td>{{ $user->email }}</td>
					<td>
						<a href="{{ route('admin.users.destroy', $user->id) }}" class="btn btn-danger" onclick=" return confirm('Seguro que deseas eliminarlo')"></a>
						<a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-warning"></a>
					</td>
				</tr>
			@endforeach	
		</tbody>    
	  </table>
	  	{!! $users->render(); !!}
	</div>

@endsection