@extends('admin.template.main')

@section('title', 'Editar usuario '.$users->name)

	@section('contenido')

	{!! Form::open(['route' => ['users.update', $users->id], 'method' => 'PUT']) !!}
		<div class="form-group">
		{!! Form::label('name', 'Nombre') !!}
		{!! Form::text('name', $users->name, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('email', 'Correo elctrónico') !!}
		{!! Form::email('email', $users->email, ['class' => 'form-control', 'placeholder' => 'Ingrese correo', 'required']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('type', 'Tipo') !!}
		{!! Form::select('type',['' => 'Seleccione tipo usuario', 'member' => 'Miembro', 'admin' => 'Administrador'], $users->type, ['class="form-control"']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
		</div>


	{!! Form::close() !!}



	@endsection
