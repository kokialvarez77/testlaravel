@extends('admin.template.main')

@section('title', 'Crear usuario')

	@section('contenido')


	{!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
		<div class="form-group">
		{!! Form::label('name', 'Nombre') !!}
		{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('email', 'Correo elctrónico') !!}
		{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Ingrese correo', 'required']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('password', 'Ingrese clave') !!}
		{!! Form::password('password', ['class' => 'form-control', 'placeholder' => '********', 'required']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('type', 'Tipo') !!}
		{!! Form::select('type',['' => 'Seleccione tipo usuario', 'member' => 'Miembro', 'admin' => 'Administrador'], null, ['class="form-control"']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Ingresar', ['class' => 'btn btn-primary']) !!}
		</div>


	{!! Form::close() !!}



	@endsection
