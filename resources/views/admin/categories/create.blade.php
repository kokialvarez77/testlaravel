@extends('admin.template.main')

@section('title', 'Agregar categoria')

@section('contenido')
	
	{!! Form::open(['route' => 'categories.store', 'method' => 'POST']) !!}
		<div class="form-group">
			{!! Form::label('name', 'Nombre') !!}
			{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre de categoría', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Ingresar', ['class' => 'btn btn-primary']) !!}
		</div>
	{!! Form::close() !!}

@endsection
