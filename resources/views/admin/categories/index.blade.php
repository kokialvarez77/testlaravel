@extends('admin.template.main')

@section('title', 'Lista de Categorías')

@section('contenido')
	<a href="{{ route('categories.create') }}" class="btn btn-info"> Nueva Categoria </a> <br>
	<div class="table-responsive">
	  <table class="table table-striped">
	  	<thead>
		    <th> ID </th>
		    <th> Nombre </th>
		    <th> ACCION </th>
		</thead>
		<tbody>
			@foreach($categories as $category)
				<tr>
					<td>{{ $category->id }}</td>
					<td>{{ $category->name }}</td>
					<td>
						<a href="{{ route('admin.categories.destroy', $category->id) }}" class="btn btn-danger" onclick=" return confirm('Seguro que deseas eliminarlo')"></a>
						<a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-warning"></a>	
					</td>
				</tr>
			@endforeach	
		</tbody>    
	  </table>
	  	{!! $categories->render(); !!}
	</div>

@endsection