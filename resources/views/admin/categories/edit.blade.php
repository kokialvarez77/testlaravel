@extends('admin.template.main')

@section('title', 'Editar Categoría '.$categories->name)

	@section('contenido')

	{!! Form::open(['route' => ['categories.update', $categories->id], 'method' => 'PUT']) !!}
		<div class="form-group">
		{!! Form::label('name', 'Nombre') !!}
		{!! Form::text('name', $categories->name, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
		</div>


	{!! Form::close() !!}



	@endsection