<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Laracat\Flash\Flash;
use App\Http\Requests\UserRequest;

class UsersController extends Controller
{
	public function index(){
		$users = User::orderBy('id', 'ASC')->paginate(10);
		return view('admin.users.index')->with('users', $users);
	}

    public function create(){
    	return view('admin.users.create');
    }

    public function store(UserRequest $request){
    	
    	$user = new User($request->all());
    	$user->password = bcrypt($request->password);
    	$user->save();

    	flash('Usuario creado '.$user->name, 'success');
    	return redirect()->route('users.index');

    	//return view('admin.users.index');
    	//dd('Usuario creado');

    }

    public function destroy($id){

    	$user = User::find($id);
    	$user->delete();
    	flash('Usuario ' .$user->name. ' ha sido borrado', 'danger');
    	return redirect()->route('users.index');
    }

    public function edit($id){

        $user = User::find($id);
        return view('admin.users.edit')->with('users', $user);
    }

    public function update(Request $request, $id){
        
        $user = User::find($id);
        $user->fill($request->all());
        // $user->name = $request->name;
        // $user->email = $request->email;
        // $user->type = $request->type;
        $user->save();

        flash('Usuario '.$user->name. ' actualizado', 'success');
        return redirect()->route('users.index');
    }
}
