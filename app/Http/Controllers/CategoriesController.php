<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Category;
use Laracat\Flash\Flash;

class CategoriesController extends Controller
{	
	public function index(){
		$category = Category::orderBy('id', 'ASC')->paginate(10);
		return view('admin.categories.index')->with('categories', $category);
	}
    
    public function create(){

    	return view('admin/categories.create');
    }

    public function store(CategoryRequest $request){

    	$category = new Category($request->all());
    	$category->save();
    	
    	flash('La categoría '.$category->name. ' ha sido creada', 'success');
    	return redirect()->route('categories.index');
    }

    public function destroy($id){

        $category = Category::find($id);
        $category->delete();
        flash('Categoría ' .$category->name. ' ha sido borrada', 'danger');
        return redirect()->route('categories.index');
    }

    public function edit($id){

        $category = Category::find($id);
        return view('admin.categories.edit')->with('categories', $category);
    }

    public function update(Request $request, $id){
    	
        $category = Category::find($id);
        $category->fill($request->all());
        $category->save();

        flash('Categoría '.$category->name. ' actualizada', 'success');
        return redirect()->route('categories.index');
    }
}
